# README #

Repository for the Wang Lab introduction to GWAS visualization project.



## Setup

#### 1. Install Jupyter via Anaconda distribution  
* https://www.anaconda.com/products/individual#Downloads  
* Anaconda installation should include:  
	- JupyterLab  
	- Pandas  
	- \+ much more  
* If you don’t have enough disk space for the full Anaconda distribution, you can use the Miniconda installer instead:  
	- https://docs.conda.io/en/latest/miniconda.html      

<br>



#### 2. Clone | download gwas_viz_project Bitbucket repository  

* https://bitbucket.org/elimly/gwas_viz_project  


* Option 1: Clone repository  
	- _Requires Git_  
	- Open terminal & cd to desired local directory  
	- Paste git clone command into terminal and run:  

		```git clone https://elimly@bitbucket.org/elimly/gwas_viz_project.git```  
<br>  

* Option 2: Download repository  

	- Click on Download repository (above)  

	- Move downloaded zip’d file to desired directory  
	- Uncompress directory     
	

<br>



#### 3. Install Python & R dependencies via conda  


* Open terminal & cd to local gwas_viz_project directory containing `environment.yml` file  

* Create new Conda environment for GWAS data viz project:  
	
	```conda env create -f environment.yml```  
<br>  
	

* To activate conda environment:  
	
	```conda activate gwasviz```


	  
___  


## How to run  

#### 1. Open terminal & cd to local gwas_viz_project directory    


#### 2. Activate conda environment     

* In terminal:  
    
	```conda activate gwasviz```

  



#### 3. Launch Jupyter Lab     

* From terminal:  
    
	```jupyter lab```   
<br>  

* The above command should automatically open web browser window  
* If not, copy & paste link from terminal into new web browser window	
* _Recommended browser = Chrome_




<br>  

___

#### See project Google Doc for additional information:  

https://docs.google.com/document/d/1rJmwFLPih4JnZvC_deJXGDEDygcrXaRbHloZtWMtgoo/edit?usp=sharing 


